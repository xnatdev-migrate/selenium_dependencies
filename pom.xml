<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.7.0</version>
    </parent>

    <artifactId>selenium_dependencies</artifactId>
    <version>4.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>NRG Selenium Dependencies</name>
    <description>Dependencies to grab for use in Selenium projects</description>

    <scm>
        <connection>scm:hg:https://bitbucket.org/xnatdev/selenium_dependencies</connection>
        <url>scm:hg:https://bitbucket.org/xnatdev/selenium_dependencies</url>
    </scm>

    <properties>
        <selenium.version>2.53.1</selenium.version>
        <nrg_selenium.version>4.0-SNAPSHOT</nrg_selenium.version>
        <test_listeners.version>1.2-SNAPSHOT</test_listeners.version>
        <commons_math.version>3.2</commons_math.version>
        <commons_io.version>2.5</commons_io.version>
        <rest_assured.version>2.4.1</rest_assured.version>
        <javax_mail.version>1.5.4</javax_mail.version>
        <testng.version>6.9.10</testng.version>
        <opencsv.version>2.0</opencsv.version>
        <jsoup.version>1.7.1</jsoup.version>
        <chromemanager.version>1.0.0</chromemanager.version>
        <dcmsnd.version>2.0.29</dcmsnd.version>
        <zip4j.version>1.3.2</zip4j.version>
        <javassist.version>3.20.0-GA</javassist.version>
        <nrg_imagej.version>2.0.0-rc-43</nrg_imagej.version>
        <bio_image.version>5.2.4</bio_image.version>
        <jobshop.version>1.0</jobshop.version>
        <spring_jdbc.version>4.2.5.RELEASE</spring_jdbc.version>
        <psql_driver.version>9.1-901-1.jdbc4</psql_driver.version>
        <sshj.version>0.17.2</sshj.version>
        <xml_apis.version>1.4.01</xml_apis.version>
    </properties>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>org.nrg</groupId>
                <artifactId>selenium</artifactId>
                <version>${nrg_selenium.version}</version>
            </dependency>

            <dependency>
                <groupId>org.nrg</groupId>
                <artifactId>test_listeners</artifactId>
                <version>${test_listeners.version}</version>
            </dependency>

            <dependency>
                <groupId>org.nrg</groupId>
                <artifactId>jobshop</artifactId>
                <version>${jobshop.version}</version>
            </dependency>

            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-java</artifactId>
                <version>${selenium.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-math3</artifactId>
                <version>${commons_math.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${commons_io.version}</version>
            </dependency>

            <dependency>
                <groupId>com.jayway.restassured</groupId>
                <artifactId>rest-assured</artifactId>
                <version>${rest_assured.version}</version>
            </dependency>

            <dependency>
                <groupId>com.sun.mail</groupId>
                <artifactId>javax.mail</artifactId>
                <version>${javax_mail.version}</version>
            </dependency>

            <dependency>
                <groupId>org.testng</groupId>
                <artifactId>testng</artifactId>
                <version>${testng.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>net.sf.opencsv</groupId>
                <artifactId>opencsv</artifactId>
                <version>${opencsv.version}</version>
            </dependency>

            <dependency>
                <groupId>org.jsoup</groupId>
                <artifactId>jsoup</artifactId>
                <version>${jsoup.version}</version>
            </dependency>

            <dependency>
                <groupId>io.github.bonigarcia</groupId>
                <artifactId>webdrivermanager</artifactId>
                <version>${chromemanager.version}</version>
            </dependency>

            <dependency>
                <groupId>dcm4che.tool</groupId>
                <artifactId>dcm4che-tool-dcmsnd</artifactId>
                <version>${dcmsnd.version}</version>
            </dependency>

            <dependency>
                <groupId>net.lingala.zip4j</groupId>
                <artifactId>zip4j</artifactId>
                <version>${zip4j.version}</version>
            </dependency>

            <dependency>
                <groupId>org.javassist</groupId>
                <artifactId>javassist</artifactId>
                <version>${javassist.version}</version>
            </dependency>

            <dependency>
                <groupId>net.imagej</groupId>
                <artifactId>imagej</artifactId>
                <version>${nrg_imagej.version}</version>
            </dependency>

            <dependency>
                <groupId>ome</groupId>
                <artifactId>formats-gpl</artifactId>
                <version>${bio_image.version}</version>
            </dependency>

            <dependency>
                <groupId>ome</groupId>
                <artifactId>bio-formats_plugins</artifactId>
                <version>${bio_image.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>ch.qos.logback</groupId>
                        <artifactId>logback-classic</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jdbc</artifactId>
                <version>${spring_jdbc.version}</version>
            </dependency>

            <dependency>
                <groupId>postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${psql_driver.version}</version>
            </dependency>

            <dependency>
                <groupId>com.hierynomus</groupId>
                <artifactId>sshj</artifactId>
                <version>${sshj.version}</version>
            </dependency>

            <dependency>
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis</artifactId>
                <version>${xml_apis.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <repositories>
        <repository>
            <id>org.nrg.xnat.maven.libraries.release</id>
            <name>XNAT Maven Release Libraries</name>
            <url>http://nrgxnat.jfrog.io/nrgxnat/libs-release</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.xnat.maven.libraries.snapshot</id>
            <name>XNAT Maven Snapshot Libraries</name>
            <url>http://nrgxnat.jfrog.io/nrgxnat/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.maven.artifacts.remote</id>
            <name>XNAT Remote Maven Repo</name>
            <url>http://nrgxnat.jfrog.io/nrgxnat/remote-repos</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

</project>